package com.kelompok1.e_lapcom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ProfileFragment extends Fragment {
    private ImageButton edit;
    TextView textViewName, textViewUname, textViewGender, textViewAdd, textViewE, textViewP;
    Context context;

    public ProfileFragment(){}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);

//        if (!SharedPrefmanager.getInstance(getActivity().getApplicationContext()).isLoggedIn()) {
//            startActivity(new Intent(getActivity().getApplicationContext(), Login.class));
//        }

        textViewName = (TextView) view.findViewById(R.id.txtName);
        textViewUname = (TextView) view.findViewById(R.id.txtUname);
        textViewGender = (TextView) view.findViewById(R.id.txtGender);
        textViewAdd = (TextView) view.findViewById(R.id.txtAddress);
        textViewE = (TextView) view.findViewById(R.id.txtEmail);
        textViewP = (TextView) view.findViewById(R.id.txtPhone);

        User user = SharedPrefmanager.getInstance(getActivity().getApplicationContext()).getUser();

        textViewName.setText(user.getNama_user());
        textViewUname.setText(user.getUsername());
        textViewGender.setText(user.getJenis_kelamin());
        textViewAdd.setText(user.getAlamat());
        textViewE.setText(user.getEmail());
        textViewP.setText(user.getNo_hp());

       edit = (ImageButton) view.findViewById(R.id.edit5);
       edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), Change_profile.class);
               startActivity(intent);
            }
        });
        return view;
    }
}
