package com.kelompok1.e_lapcom;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.security.PrivateKey;

public class HomeFragment extends Fragment {
    //RecyclerView mRecyclerView;
    //RecyclerView.LayoutManager mLayoutManager;
    //RecyclerView.Adapter mAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    CardView cardfirst;
    ViewFlipper vFlipper;
    int[] images = {
            R.drawable.banner4,
            R.drawable.banner2,
            R.drawable.banner5
    };

    String[] kategori = {
            "Notebook",
            "iPhone",
            "Kamera Digital",
            "Headset",
            "Monitor",
            "Keyboard",
            "Mouse",
            "Memory",
            "Speaker",
            "Lainnya"

    };

    int[] image_kategori = {
            R.drawable.ic_computer,
            R.drawable.ic_iphone,
            R.drawable.ic_camera,
            R.drawable.ic_headset_black_24dp,
            R.drawable.ic_desktop_windows_black_24dp,
            R.drawable.ic_keyboard,
            R.drawable.ic_mouse,
            R.drawable.ic_memory_black_24dp,
            R.drawable.ic_speaker_black_24dp,
            R.drawable.ic_widgets_black_24dp

    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_home, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ViewFlipper viewFlipper = (ViewFlipper) view.findViewById(R.id.bannerSlider);
        viewFlipper.startFlipping();

        cardfirst = (CardView) view.findViewById(R.id.cardpertama);
        cardfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), DetailProduct.class);
                startActivity(intent);
            }
        });
        //mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
       // mRecyclerView.setHasFixedSize(true);

        //mLayoutManager  = new GridLayoutManager(getActivity(),2);
        //mRecyclerView.setLayoutManager(mLayoutManager);

        //mRecyclerView.setAdapter(mAdapter);





        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_toolbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


}
