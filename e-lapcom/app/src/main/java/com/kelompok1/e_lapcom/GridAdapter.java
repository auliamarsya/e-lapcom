package com.kelompok1.e_lapcom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter{

    Context context;

    private final String[] kategori;
    private final int [] image_kategori;
    View view;
    LayoutInflater layoutInflater;

    public GridAdapter(Context context, String[] kategori, int[] image_kategori) {
        this.context = context;
        this.kategori = kategori;
        this.image_kategori = image_kategori;
    }

    @Override
    public int getCount() {
        return kategori.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null){
            view = new View(context);
            view = layoutInflater.inflate(R.layout.single_item, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
            TextView textView = (TextView) view.findViewById(R.id.textvieww);
            imageView.setImageResource(image_kategori[position]);
            textView.setText(kategori[position]);
        }
        return view;
    }
}
