package com.kelompok1.e_lapcom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
public class Regis_1 extends AppCompatActivity {
    Button next;
    EditText et_name, et_phone, et_uname, et_pass, et_cpass;
    String valid_n, valid_p, valid_u, valid_pass, valid_cp,token;
    String name, phone, uname, pass, cpass;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regis_1);
        next = (Button)findViewById(R.id.next2);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validasi();
                    requestData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        et_name = (EditText)findViewById(R.id.name);
        et_phone = (EditText)findViewById(R.id.phone);
        et_uname = (EditText)findViewById(R.id.username);
        et_pass = (EditText)findViewById(R.id.password);
        et_cpass = (EditText)findViewById(R.id.cpassword);
    }

    public void validasi() throws JSONException {
        valid_n = et_name.getText().toString();
        valid_p = et_phone.getText().toString();
        valid_u = et_uname.getText().toString();
        valid_pass = et_pass.getText().toString();
        valid_cp = et_cpass.getText().toString();

        if (TextUtils.isEmpty(valid_n)) {
            et_name.setError("Input name first");
        } else if (TextUtils.isEmpty(valid_u)) {
            et_uname.setError("Input username first");
        } else if (TextUtils.isEmpty(valid_p)) {
            et_phone.setError("Input phone first");
        } else if (TextUtils.isEmpty(valid_pass)) {
            et_pass.setError("Input password first");
        } else if (TextUtils.isEmpty(valid_p)) {
            et_cpass.setError("Input your password again");
//        }else{
//            JSONObject headers = new JSONObject();
//
//            headers.put("nama_user", valid_n );
//            headers.put("username", valid_u);
//            headers.put("password", valid_pass);
//            headers.put("no_hp", valid_p);
//
//
        }
    }

    public void requestData (){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://117.53.45.240/e-lapcom/index.php/register";
        JSONObject jsonBodyObj = new JSONObject();
        try {
            jsonBodyObj.put("nama_user", valid_n);
            jsonBodyObj.put("username", valid_u);
            jsonBodyObj.put("no_hp", valid_p);
            jsonBodyObj.put("password", valid_pass);

        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "JSONError1: " + e.getMessage().toString(), Toast.LENGTH_LONG).show();

        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBodyObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), "Register Success ", Toast.LENGTH_LONG).show();
                        Intent success = new Intent(Regis_1.this,Login.class);
                        finish();
                        startActivity(success);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error: ", error.toString());
                        Toast.makeText(getApplicationContext(), "VolleyError : " + error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
//@Override
//public void onBackPressed() {
//   super.onBackPressed();
// }























//public class Regis_1 extends AppCompatActivity {
//    Button next;
//    EditText name, phone, uname, pass, cpass;
//    String valid_n, valid_p, valid_u, valid_pass, valid_cp,token;
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.regis_1);
//        next = (Button)findViewById(R.id.next2);
//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    validasi();
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        name = (EditText)findViewById(R.id.name);
//        phone = (EditText)findViewById(R.id.phone);
//        uname = (EditText)findViewById(R.id.username);
//        pass = (EditText)findViewById(R.id.password);
//        cpass = (EditText)findViewById(R.id.cpassword);
//    }
//
//    public void validasi() throws JSONException {
//        valid_n = name.getText().toString();
//        valid_p = phone.getText().toString();
//        valid_u = uname.getText().toString();
//        valid_pass = pass.getText().toString();
//        valid_cp = cpass.getText().toString();
//
//        if (TextUtils.isEmpty(valid_n)) {
//            name.setError("Input name first");
//        } else if (TextUtils.isEmpty(valid_u)) {
//            phone.setError("Input username first");
//        } else if (TextUtils.isEmpty(valid_p)) {
//            phone.setError("Input phone first");
//        } else if (TextUtils.isEmpty(valid_pass)) {
//            phone.setError("Input password first");
//        } else if (TextUtils.isEmpty(valid_p)) {
//            phone.setError("Input your password again");
////        }else{
////            JSONObject headers = new JSONObject();
////
////            headers.put("nama_user", valid_n );
////            headers.put("username", valid_u);
////            headers.put("password", valid_pass);
////            headers.put("no_hp", valid_p);
////
////
////            }
//
////            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, URLs.URL_REGISTER, headers,
////                    new Response.Listener<JSONArray>() {
////                        @Override
////                        public void onResponse(JSONArray response) {
////                          //  try {
////                                JSONArray obj = response;
////
//////                                if (!obj.getBoolean("error")) {
////                                    Toast.makeText(getApplicationContext(), "registration succeed", Toast.LENGTH_SHORT).show();
////
//////                                JSONArray userJson = obj.getJSONArray("user");
////
//////                                    User user = new User(
//////                                            userJson.getInt("id"),
//////                                            userJson.getString("username"),
//////                                            userJson.getString("email"),
//////                                            userJson.getString("nama_user"),
//////                                            userJson.getString("jenis_kelamin"),
//////                                            userJson.getString("tgl_lahir"),
//////                                            userJson.getString("alamat"),
//////                                            userJson.getString("no_hp")
//////                                    );
//////
//////                                  SharedPrefmanager.getInstance(getApplicationContext()).userLogin(user);
////                                    finish();
////                                    startActivity(new Intent(getApplicationContext(), Login.class));
//////                                } else {
////                                 Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
//////                                }
////                         //   } catch (JSONException e) {
////                               // e.printStackTrace();
////                                //Toast.makeText(getApplicationContext(),Toast.LENGTH_SHORT).show();
////                           // }
////                        }
////                    },
////                    new Response.ErrorListener() {
////                        @Override
////                        public void onErrorResponse(VolleyError error) {
////                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
////                        }
////                    }){
////                @Override
////                protected Map<String, String> getParams() throws AuthFailureError {
////                    HashMap<String, String> headers = new HashMap<>();
////
////                    return headers;
////                }
////            };
////            VolleySingleton.getmInstance(this).addToRequestQueue(stringRequest);
//        }
//
//        public void requestData (String s_uname, String ){
//            RequestQueue queue = Volley.newRequestQueue(this);
//            String url = "http://117.53.45.240/e-lapcom/index.php/user";
//            JSONObject jsonBodyObj = new JSONObject();
//            try {
//                jsonBodyObj.put("nama_user", name);
//                jsonBodyObj.put("username", uname);
//                jsonBodyObj.put("no_hp", phone);
//                jsonBodyObj.put("password", pass);
//
//            } catch (JSONException e) {
//                Toast.makeText(getApplicationContext(), "JSONError1: " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
//
//            }
//            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBodyObj,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                if (uname.compareTo(response.getString("username")) == 0) {
//                                    Toast.makeText(getApplicationContext(), "Sign up succeed", Toast.LENGTH_LONG).show();
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Failed to sign up", Toast.LENGTH_LONG).show();
//                                }
//
//                            } catch (JSONException e) {
//                                Toast.makeText(getApplicationContext(), "JSONError2:  " + e.getMessage(), Toast.LENGTH_LONG).show();
//                            }
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
////                        Log.d("Error: ", error.toString());
//                            Toast.makeText(getApplicationContext(), "VolleyError : " + error.getMessage().toString(), Toast.LENGTH_LONG).show();
//                        }
//                    }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<>();
//                    headers.put("Content-Type", "application/json");
//                    return headers;
//                }
//            };
//            queue.add(jsonObjectRequest);
//        }
//    }
//    //@Override
//    //public void onBackPressed() {
//     //   super.onBackPressed();
//   // }
//}
