package com.kelompok1.e_lapcom;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.kelompok1.e_lapcom.URLs.URL_LOGIN;

public class Login extends AppCompatActivity {
    final Context context = this;
    TextView regis;
    Button login;
    EditText uname, pass;
    String token;
    //String valid_u, valid_p;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
//
//        if (SharedPrefmanager.getInstance(this).isLoggedIn()) {
//            finish();
//            startActivity(new Intent(this, MainActivity.class));
//        }

        regis = (TextView)findViewById(R.id.register);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(Login.this,Regis_1.class);
                finish();
                startActivity(register);
            }
        });
        login = (Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasi();
                requestData(uname.getText().toString(),pass.getText().toString());
//                SharedPreferences.Editor editor = getSharedPreferences("session", MODE_PRIVATE).edit();
//                editor.putString("token", notif);
//                editor.apply();

//               SharedPreferences prefs = getSharedPreferences("session", MODE_PRIVATE);
//                String restoredText = prefs.getString("token", null);
//                requestData(restoredText);
            }
        });
        uname = (EditText)findViewById(R.id.uname);
        pass = (EditText)findViewById(R.id.pass);
    }

    public void validasi(){
        final String valid_u = uname.getText().toString();
        final String valid_p = pass.getText().toString();
        if (TextUtils.isEmpty(valid_u)){
            uname.setError("Input username first");
        }else if (TextUtils.isEmpty(valid_p)){
            pass.setError("Input password first");
        }
    }
    public void requestData(String uname, String pass){
        RequestQueue queue = Volley.newRequestQueue(this );
        String url="http://117.53.45.240/e-lapcom/index.php/rest/generate";
        JSONObject jsonBodyObj = new JSONObject();
        try{

            jsonBodyObj.put("username",uname);
            jsonBodyObj.put("password",pass);

        }catch (JSONException e){
            Toast.makeText(getApplicationContext(), "JSONError1:  " + e.getMessage(), Toast.LENGTH_LONG).show();

        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBodyObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            token = response.getString("token");
                            Toast.makeText(getApplicationContext(), "token:  " + token, Toast.LENGTH_LONG).show();
                            Intent success = new Intent(Login.this,MainActivity.class);
                            finish();
                            startActivity(success);

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "JSONError1:  " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Log.d("Error: ", error.toString());
                        //Toast.makeText(getApplicationContext(),"Gagal Login, Masukan Username dan Password yang valid ",Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "VolleyError:  " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }
//    public String requestData(String uname, String pass){
//        RequestQueue queue = Volley.newRequestQueue(this );
//        String url="http://117.53.45.240/e-lapcom/index.php/rest/generate";
//        JSONObject jsonBodyObj = new JSONObject();
//        try{
//
//            jsonBodyObj.put("username",uname);
//            jsonBodyObj.put("password",pass);
//
//        }catch (JSONException e){
//            e.printStackTrace();
//
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBodyObj,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            token = response.getString("token");
//
//                        } catch (JSONException e) {
//                            Toast.makeText(getApplicationContext(), "Error:  " + e.getMessage(), Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error: ", error.toString());
//                        Toast.makeText(getApplicationContext(),"Gagal Login, Masukan Username dan Password yang valid ",Toast.LENGTH_LONG).show();
//                    }
//                }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        queue.add(jsonObjectRequest);
//        return token;}

    @Override
    public void onBackPressed() {
        AlertDialog.Builder exit = new AlertDialog.Builder(context);
        exit.setTitle("Are you sure you want to quit?");
        exit.setMessage("Press OK to exit");
        exit.setCancelable(false);
        exit.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Login.this.finish();
            }
        });
        exit.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog exitD = exit.create();
        exitD.show();
    }

    public void requestData(final String token) { //get info credential
        //Toast.makeText(getApplicationContext(),token,Toast.LENGTH_LONG).show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://117.53.45.240/e-lapcom/index.php/rest/generate";

        JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i = 0; i < response.length();i++){
                            try {
                                JSONObject hasil = response.getJSONObject(i);
                                //Toast.makeText(getApplicationContext(),"Id:" + hasil.optInt("id_employee"),Toast.LENGTH_LONG).show();
                                SharedPreferences sp = getSharedPreferences("absensee", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
//                                Toast.makeText(getApplicationContext(), "Nama: "+hasil.optString("nama_employee")+"\nUsername: "+hasil.optString("username")
//                                        ,Toast.LENGTH_LONG).show();
                            }
                            catch (JSONException e){

                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error: ", error.toString());
                        Toast.makeText(getApplicationContext(),"Gagal Login",Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "JWT "+token);
                return headers;
            }
        };
        queue.add(jsObjRequest);//queue.add(jsObjRequest);
    }
}
