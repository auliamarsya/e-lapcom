package com.kelompok1.e_lapcom;

public class URLs {
    private static final String ROOT_URL = "http://117.53.45.240/e-lapcom";

    public static final String URL_REGISTER = ROOT_URL + "/user";
    public static final String URL_LOGIN= ROOT_URL + "/login";
}
