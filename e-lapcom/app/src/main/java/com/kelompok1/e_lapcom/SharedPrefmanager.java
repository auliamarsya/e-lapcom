package com.kelompok1.e_lapcom;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class SharedPrefmanager {
    private static final String SHARED_PREF_NAME = "simplifiedcodingsharedfref";
    private static final String KEY_USERNAME = "keyusername";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_NAMAUSER = "keyenamauser";
    private static final String KEY_GENDER = "keygender";
    private static final String KEY_TGL_LAHIR = "keytgllahir";
    private static final String KEY_ALAMAT = "keyalamat";
    private static final String KEY_NO_HP = "keynohp";
    private static final String KEY_ID = "keyid";

    private static SharedPrefmanager mInstance;
    private static Context mCtx;

    private SharedPrefmanager(Context context){
        mCtx = context;
    }

    public static synchronized SharedPrefmanager getInstance(Context context){
        if(mInstance == null){
            mInstance = new SharedPrefmanager(context);
        }
        return mInstance;
    }

    public void userLogin(User user){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putString(KEY_USERNAME, user.getUsername());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_NAMAUSER, user.getNama_user());
        editor.putString(KEY_GENDER, user.getJenis_kelamin());
        editor.putString(KEY_TGL_LAHIR, user.getTgl_lahir());
        editor.putString(KEY_ALAMAT, user.getAlamat());
        editor.putString(KEY_NO_HP, user.getNo_hp());
        editor.apply();
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null) != null;
    }

    public User getUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getInt(KEY_ID,    -1),
                sharedPreferences.getString(KEY_USERNAME, null),
                sharedPreferences.getString(KEY_EMAIL, null ),
                sharedPreferences.getString(KEY_NAMAUSER, null ),
                sharedPreferences.getString(KEY_GENDER, null),
                sharedPreferences.getString(KEY_TGL_LAHIR, null),
                sharedPreferences.getString(KEY_ALAMAT, null),
                sharedPreferences.getString(KEY_NO_HP,  null)
        );
    }

    public void logout(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        mCtx.startActivity(new Intent(mCtx, Login.class));
    }
}
