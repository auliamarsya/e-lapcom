package com.kelompok1.e_lapcom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Regis_2 extends AppCompatActivity {
    Button done;
    EditText uname, pass, cpass;
    String valid_u, valid_p, valid_cp;
    ProgressBar progressBar;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regis_2);

        done = (Button)findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasi();

            }
        });
        uname = (EditText)findViewById(R.id.uname1);
        pass = (EditText)findViewById(R.id.pass1);
        cpass = (EditText)findViewById(R.id.cpass);
    }

    public void validasi(){
        valid_u = uname.getText().toString();
        valid_p = pass.getText().toString();
        valid_cp = cpass.getText().toString();
        if(TextUtils.isEmpty(valid_u)){
            uname.setError("Input username first");
        }else if(TextUtils.isEmpty(valid_p)){
            pass.setError("Input password first");
        }else if(TextUtils.isEmpty(valid_cp)){
            cpass.setError("Input your password again");
        }else{
            Intent done = new Intent(Regis_2.this,Login.class);
            finish();
            startActivity(done);
        }
    }

/*    public void ambildata(){
        final String username = valid_u.trim();
        final String password = valid_cp.trim();

    }*/

    /*StringRequest stringRequest = new StringRequest(Request.Method.POST, "117.53.45.240/e-lapcom/user.php", null){
        new Response.Listener<String>() {
            @Override
            public void onResponse(String respone) {
                progressBar.setVisibility(View.GONE);

                try {
                    //converting response to json object
                    JSONObject obj = new JSONObject(response);

                    //if no error in response
                    if (!obj.getBoolean("error")) {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        //getting the user from the response
                        JSONObject userJson = obj.getJSONObject("user");

                        //creating a new user object
                        User user = new User(
                                userJson.getInt("id_user"),
                                userJson.getString("username"),
                                userJson.getString("email"),
                                userJson.getString("gender")
                        );

                        //storing the user in shared preferences
                        SharedPrefmanager.getInstance(getApplicationContext()).userLogin(user);

                        //starting the profile activity
                        finish();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", uname);
                params.put("password", password);
                params.put("gender", gender);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
    }*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
